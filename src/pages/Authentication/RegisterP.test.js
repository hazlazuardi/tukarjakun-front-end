import React from 'react';
import { render, screen } from '../../test-utils';
// import { render } from '@testing-library/react';
import RegisterP from './RegisterP';

it("checkUsernameFormInRegisterRendered", () => {
	const { queryByTitle } = render(<RegisterP />)
	const usernameForm = queryByTitle("usernameForm");
	expect(usernameForm).toBeTruthy();
})

it("checkPasswordFormInRegisterRendered", () => {
	const { queryByTitle } = render(<RegisterP />)
	const passwordForm = queryByTitle("passwordForm");
	expect(passwordForm).toBeTruthy();
})

it("checkRolesFormInRegisterRendered", () => {
	const { queryByTitle } = render(<RegisterP />)
	const rolesOptions = queryByTitle("rolesOptions");
	expect(rolesOptions).toBeTruthy();
})

it("checkLoginButtonInRegisterRendered", () => {
	const { queryByTitle } = render(<RegisterP />)
	const registerBtn = queryByTitle("registerBtn");
	expect(registerBtn).toBeTruthy();
})

it("checkDontHaveAccountButtonInRegisterRendered", () => {
	const { queryByTitle } = render(<RegisterP />)
	const loginBtn = queryByTitle("loginInRegisterBtn");
	expect(loginBtn).toBeTruthy();
})