import React from 'react';
import { render, screen } from '../../test-utils';
// import { render } from '@testing-library/react';
import UserLoginP from './UserLoginP';

it("checkUsernameFormInLoginRendered", () => {
	const { queryByTitle } = render(<UserLoginP />)
	const registerBtn = queryByTitle("usernameForm");
	expect(registerBtn).toBeTruthy();
})

it("checkPasswordFormInLoginRendered", () => {
	const { queryByTitle } = render(<UserLoginP />)
	const registerBtn = queryByTitle("passwordForm");
	expect(registerBtn).toBeTruthy();
})

it("checkLoginButtonInLoginRendered", () => {
	const { queryByTitle } = render(<UserLoginP />)
	const loginBtn = queryByTitle("loginBtn");
	expect(loginBtn).toBeTruthy();
})

it("checkDontHaveAccountButtonInLoginRendered", () => {
	const { queryByTitle } = render(<UserLoginP />)
	const registerBtn = queryByTitle("registerInLoginBtn");
	expect(registerBtn).toBeTruthy();
})